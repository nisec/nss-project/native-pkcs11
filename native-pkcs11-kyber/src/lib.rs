// Copyright 2022 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//! Keep this module empty to prevent breaking the workspace-wide build on other
//! platforms.

use std::sync::Arc;

use native_pkcs11_traits::Backend;

pub type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

#[derive(Debug)]
struct PublicKey(pqc_kyber::PublicKey);

#[derive(Debug)]
struct PrivateKey(pqc_kyber::SecretKey);

impl native_pkcs11_traits::PublicKey for PublicKey {
    fn public_key_hash(&self) -> Vec<u8> {
        (*blake3::hash(&self.0).as_bytes()).into()
    }
    fn label(&self) -> String {
        "kyberkey".into()
    }
    fn to_der(&self) -> Vec<u8> {
        self.0.into()
    }
    fn verify(
        &self,
        algorithm: &native_pkcs11_traits::SignatureAlgorithm,
        data: &[u8],
        signature: &[u8],
    ) -> Result<()> {
        unimplemented!();
    }
    fn delete(self: Box<Self>) {
        ()
    }
    fn algorithm(&self) -> native_pkcs11_traits::KeyAlgorithm {
        native_pkcs11_traits::KeyAlgorithm::Kyber
    }
}

impl native_pkcs11_traits::PrivateKey for PrivateKey {
    fn public_key_hash(&self) -> Vec<u8> {
        native_pkcs11_traits::PublicKey::public_key_hash(&PublicKey(pqc_kyber::public(&self.0)))
    }

    fn label(&self) -> String {
        "kyberkey".into()
    }
    fn sign(
        &self,
        algorithm: &native_pkcs11_traits::SignatureAlgorithm,
        data: &[u8],
    ) -> Result<Vec<u8>> {
        unimplemented!();
    }

    fn delete(&self) {
        ()
    }

    fn algorithm(&self) -> native_pkcs11_traits::KeyAlgorithm {
        native_pkcs11_traits::KeyAlgorithm::Kyber
    }

    fn find_public_key(
        &self,
        backend: &dyn Backend,
    ) -> Result<Option<Box<dyn native_pkcs11_traits::PublicKey>>> {
        Ok(None)
    }
}

pub struct KyberBackend {}
impl Backend for KyberBackend {
    fn find_all_certificates(
        &self,
    ) -> native_pkcs11_traits::Result<Vec<Box<dyn native_pkcs11_traits::Certificate>>> {
        // let store_name = OsString::from_str(STORE_NAME)?;
        // let store = CertificateStores::GetStoreByName(&store_name.into()).unwrap();
        //
        // todo!()
        unimplemented!();
    }

    fn find_private_key(
        &self,
        _query: native_pkcs11_traits::KeySearchOptions,
    ) -> native_pkcs11_traits::Result<Option<Arc<dyn native_pkcs11_traits::PrivateKey>>> {
        Ok(None)
    }

    fn find_public_key(
        &self,
        _query: native_pkcs11_traits::KeySearchOptions,
    ) -> native_pkcs11_traits::Result<Option<Box<dyn native_pkcs11_traits::PublicKey>>> {
        Ok(None)
    }

    fn find_all_private_keys(
        &self,
    ) -> native_pkcs11_traits::Result<Vec<Arc<dyn native_pkcs11_traits::PrivateKey>>> {
        Ok(vec![])
    }

    fn find_all_public_keys(
        &self,
    ) -> native_pkcs11_traits::Result<Vec<Arc<dyn native_pkcs11_traits::PublicKey>>> {
        Ok(vec![])
    }

    fn generate_key(
        &self,
        algorithm: native_pkcs11_traits::KeyAlgorithm,
        label: Option<&str>,
    ) -> native_pkcs11_traits::Result<Arc<dyn native_pkcs11_traits::PrivateKey>> {
        if algorithm == native_pkcs11_traits::KeyAlgorithm::Kyber {
            let keys = pqc_kyber::keypair(&mut rand::thread_rng()).map_err(|e| e.to_string())?;
            Ok(Arc::new(PrivateKey(keys.secret)))
        } else {
            Err("Not capable of generating a key of this algorithm")?
        }
    }

    fn name(&self) -> String {
        "KYBER".into()
    }
}
