// Copyright 2022 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//! Keep this module empty to prevent breaking the workspace-wide build on other
//! platforms.

use std::sync::Arc;

use byteorder::ByteOrder;
use crystals_dilithium::dilithium2;
use native_pkcs11_traits::Backend;
use rand::Rng;

pub type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

struct PublicKey(dilithium2::PublicKey);

impl std::fmt::Debug for PublicKey {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("PublicKey")
            .field(
                "hash",
                &native_pkcs11_traits::PublicKey::public_key_hash(self),
            )
            .finish()
    }
}

struct PrivateKey(dilithium2::Keypair);

impl From<PrivateKey> for Vec<u8> {
    fn from(pk: PrivateKey) -> Self {
        pk.0.to_bytes().into()
    }
}

impl From<Vec<u8>> for PrivateKey {
    fn from(bytes: Vec<u8>) -> Self {
        PrivateKey(dilithium2::Keypair::from_bytes(&bytes))
    }
}

impl std::fmt::Debug for PrivateKey {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("PrivateKey")
            .field(
                "hash",
                &native_pkcs11_traits::PrivateKey::public_key_hash(self),
            )
            .finish()
    }
}

impl native_pkcs11_traits::PublicKey for PublicKey {
    fn public_key_hash(&self) -> Vec<u8> {
        blake3::hash(&self.0.bytes).as_bytes().into()
    }
    fn label(&self) -> String {
        "dilithiumkey".into()
    }
    fn to_der(&self) -> Vec<u8> {
        self.0.to_bytes().into()
    }
    fn verify(
        &self,
        algorithm: &native_pkcs11_traits::SignatureAlgorithm,
        data: &[u8],
        signature: &[u8],
    ) -> Result<()> {
        if self.0.verify(data, signature) {
            Ok(())
        } else {
            Err("Verification failed".into())
        }
    }
    fn delete(self: Box<Self>) {
        ()
    }
    fn algorithm(&self) -> native_pkcs11_traits::KeyAlgorithm {
        native_pkcs11_traits::KeyAlgorithm::CrystalsDilithium
    }
}

impl native_pkcs11_traits::PrivateKey for PrivateKey {
    fn public_key_hash(&self) -> Vec<u8> {
        native_pkcs11_traits::PublicKey::public_key_hash(&PublicKey(
            dilithium2::PublicKey::from_bytes(&self.0.public.to_bytes()),
        ))
    }

    fn label(&self) -> String {
        "dilithiumkey".into()
    }
    fn sign(
        &self,
        algorithm: &native_pkcs11_traits::SignatureAlgorithm,
        data: &[u8],
    ) -> Result<Vec<u8>> {
        let signature = self.0.secret.sign(data);
        Ok(signature.into())
    }

    fn delete(&self) {
        ()
    }

    fn algorithm(&self) -> native_pkcs11_traits::KeyAlgorithm {
        native_pkcs11_traits::KeyAlgorithm::CrystalsDilithium
    }

    fn find_public_key(
        &self,
        backend: &dyn Backend,
    ) -> Result<Option<Box<dyn native_pkcs11_traits::PublicKey>>> {
        Ok(None)
    }
}

pub struct CrystalsDilithiumBackend {
    db: parity_db::Db,
}

impl Default for CrystalsDilithiumBackend {
    fn default() -> Self {
        let db = parity_db::Db::open_or_create(&parity_db::Options::with_columns(
            "/tmp/pkcs11_dilithium".as_ref(),
            1,
        ))
        .unwrap();
        CrystalsDilithiumBackend { db }
    }
}

impl Backend for CrystalsDilithiumBackend {
    fn find_all_certificates(
        &self,
    ) -> native_pkcs11_traits::Result<Vec<Box<dyn native_pkcs11_traits::Certificate>>> {
        // let store_name = OsString::from_str(STORE_NAME)?;
        // let store = CertificateStores::GetStoreByName(&store_name.into()).unwrap();
        //
        // todo!()
        Ok(vec![])
    }

    fn find_private_key(
        &self,
        _query: native_pkcs11_traits::KeySearchOptions,
    ) -> native_pkcs11_traits::Result<Option<Arc<dyn native_pkcs11_traits::PrivateKey>>> {
        Ok(None)
    }

    fn find_public_key(
        &self,
        _query: native_pkcs11_traits::KeySearchOptions,
    ) -> native_pkcs11_traits::Result<Option<Box<dyn native_pkcs11_traits::PublicKey>>> {
        Ok(None)
    }

    fn find_all_private_keys(
        &self,
    ) -> native_pkcs11_traits::Result<Vec<Arc<dyn native_pkcs11_traits::PrivateKey>>> {
        let mut keys = vec![];
        self.db
            .iter_column_while(0, |is| {
                let value = is.value;
                let keypair: PrivateKey = value.into();
                let pk: Arc<dyn native_pkcs11_traits::PrivateKey> = Arc::new(keypair);
                keys.push(pk);
                true
            })
            .unwrap();
        Ok(keys)
    }

    fn find_all_public_keys(
        &self,
    ) -> native_pkcs11_traits::Result<Vec<Arc<dyn native_pkcs11_traits::PublicKey>>> {
        let mut keys = vec![];
        self.db
            .iter_column_while(0, |is| {
                let value = is.value;
                let keypair: PrivateKey = value.into();
                let pk: Arc<dyn native_pkcs11_traits::PublicKey> =
                    Arc::new(PublicKey(keypair.0.public));
                keys.push(pk);
                true
            })
            .unwrap();
        Ok(keys)
    }

    fn generate_key(
        &self,
        algorithm: native_pkcs11_traits::KeyAlgorithm,
        label: Option<&str>,
    ) -> native_pkcs11_traits::Result<Arc<dyn native_pkcs11_traits::PrivateKey>> {
        if algorithm == native_pkcs11_traits::KeyAlgorithm::CrystalsDilithium {
            let keys = dilithium2::Keypair::generate(None);
            Ok(Arc::new(PrivateKey(keys)))
        } else {
            Err("Not capable of generating a key of this algorithm")?
        }
    }

    fn generate_key_pair(
        &self,
        algorithm: native_pkcs11_traits::KeyAlgorithm,
        label: Option<&str>,
    ) -> native_pkcs11_traits::Result<Arc<dyn native_pkcs11_traits::PrivateKey>> {
        if algorithm == native_pkcs11_traits::KeyAlgorithm::CrystalsDilithium {
            let keys = dilithium2::Keypair::generate(None);
            let handle: u32 = rand::random();
            let mut handle_as_key = vec![0; 4];
            byteorder::LittleEndian::write_u32(&mut handle_as_key[0..4], handle);
            let keys_bytes = keys.to_bytes();
            let private_key = PrivateKey(dilithium2::Keypair::from_bytes(&keys_bytes));
            let op = (0, handle_as_key, Some(keys_bytes.into()));
            self.db.commit([op]).unwrap();
            let ret = Arc::new(private_key);
            Ok(ret)
        } else {
            Err("Not capable of generating a key of this algorithm".into())
        }
    }

    fn name(&self) -> String {
        "CRYSTALS_DILITHIUM".into()
    }
}
